#include <iostream>
#include <ANN.h>
using namespace std;
using namespace ANN;

int main()
{
	cout << "hello Tanya!" << endl;
	cout << GetTestString().c_str() << endl;
	vector <size_t> conf = { 2,3,3,1 };
	auto pAnn = CreateNeuralNetwork(conf);
	vector  < vector < float >> inputs, outputs;
	bool succes = LoadData ("xor.data", inputs, outputs);
	BackPropTraining(pAnn, inputs, outputs, 100000, 1.e-2, 0.1, true);
	pAnn->Save("xor.ann");

	return 0;
}